set t_Co=256
""""""""""""""""""""""""""" airline
let g:airline_theme="badcat"
"set langmenu=zh_CN.UTF-8
"set termencoding=cp936
"language messages zh_CN.utf-8
set helplang=cn
set ambiwidth=double
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
"设置切换Buffer快捷键"
"nnoremap <C-tab> :bn<CR>
"nnoremap <C-s-tab> :bp<CR>
"关闭状态显示空白符号计数
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#whitespace#symbol = '!'
"设置consolas字体"前面已经设置过
set guifont=Consolas\ for\ Powerline\ FixedD:h11
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '������'
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.maxlinenr = '㏑'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
" let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.maxlinenr = ''

" old vim-powerline symbols
let g:airline_left_sep = '⮀'
let g:airline_left_alt_sep = '⮁'
let g:airline_right_sep = '⮂'
let g:airline_right_alt_sep = '⮃'
let g:airline_symbols.branch = '⭠'
let g:airline_symbols.readonly = '⭤'
" let g:airline_symbols.linenr = '⭡'
let g:airline_symbols.linenr = ''

"""""""""""""""""""""""""""""""""""Vundle
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'morhetz/gruvbox'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'honza/vim-snippets'
Plugin 'tpope/vim-surround'
Plugin 'flazz/vim-colorschemes'
Plugin 'python-mode/python-mode'
Plugin 'vim-syntastic/syntastic'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
" Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
call vundle#end()
filetype plugin indent on

""""""""""""""""""""""""""""""" 主题相关. 
" 可选的主题名, 参见 .vim/bundle/vim-colorschemes/colors 目录
" colorscheme atom
" colorscheme molokai
colorscheme luna-term
" colorscheme gruvbox
" set background=dark
" set background=light
" let g:gruvbox_contrast_light="light"
" let g:gruvbox_contrast_dark="hard"
" let g:gruvbox_improved_warnings=1

"""""""""""""""""""""""""""""""""""""""碎碎念设置
" 不工作在兼容模式下
set nocp
set hlsearch
let mapleader = ";" 
set nu
behave mswin
syntax on
set si
set ai
set nowritebackup
set backspace=indent,eol,start
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set fileencodings=utf-8,gbk,cp936,cp950,latin1
set encoding=utf-8
set ruler
set showcmd
set incsearch
" 不吱吱叫
set noeb vb t_vb=
set cursorline

" 文件修改之后自动载入
set autoread
" 在上下移动光标时，光标的上方或下方至少会保留显示的行数
set scrolloff=5

" 左下角不显示当前vim模式, 这个目前需要去掉
" set noshowmode

" 调整窗口移动
nnoremap H <C-w>h
nnoremap J <C-w>j
nnoremap K <C-w>k
nnoremap L <C-w>l

" 配置快速保存
nnoremap <space>w :w<cr>
inoremap jk <esc>:w<cr>
" 这个配置需要去掉, 否则在 vim 中会导致方向键在插入模式下失效(neovim不受影响)
" inoremap <esc> <esc>:w<cr> 

" 可视模式下能够连续修改缩进
vnoremap < <gv
vnoremap > >gv

" 指定文件名打开新标签页
nnoremap <space>e :tabe 

""""""""""""""""""""""""""""""""""""""""""""""" ctags 代码跳转
"imap <C-j> <c-x><c-p>
nmap <C-l> <c-o>
nmap <C-k> <c-]>
nnoremap <F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q<cr>

""""""""""""""""""""""""""""""""""""""""""""""" 函数列表
nmap <F6> :TagbarToggle<CR>
let g:tagbar_ctags_bin='/usr/bin/ctags'
let g:tagbar_width=20
let g:tagbar_left=1
let g:tagbar_sort = 0

""""""""""""""""""""""""""""""""""""""""""""""" Dox
map <F8> :Dox<RETURN><ESC>

""""""""""""""""""""""""""""""""""""""""""""""" NERDTree
map <F7> :NERDTreeToggle<RETURN>
imap <F7> :NERDTreeToggle<RETURN>
let g:NERDTreeWinPos="right"
let g:NERDTreeWinSize=20

""""""""""""""""""""""""""""""""""""""""""""""" CtrlP
let g:ctrlp_working_path_mode='a'

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

""""""""""""""""""""""""""""""""""""""""""""""" cpp-enhanced-highlight
" 高亮类，成员函数，标准库和模板
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_concepts_highlight = 1
let g:cpp_experimental_simple_template_highlight = 1
" 文件较大时使用下面的设置高亮模板速度较快，但会有一些小错误
let g:cpp_experimental_template_highlight = 1

""""""""""""""""""""""""""""""""""""""""""""""" 按 zz 折叠代码
set foldenable
" 折叠方法
" manual    手工折叠
" indent    使用缩进表示折叠
" expr      使用表达式定义折叠
" syntax    使用语法定义折叠
" diff      对没有更改的文本进行折叠
" marker    使用标记进行折叠, 默认标记是 {{{ 和 }}}
set foldmethod=indent
set foldlevel=99

" 代码折叠自定义快捷键 zz
let g:FoldMethod = 0
noremap zz :call ToggleFold()<cr>
fun! ToggleFold()
    if g:FoldMethod == 0
        exe "normal! zM"
        let g:FoldMethod = 1
    else
        exe "normal! zR"
        let g:FoldMethod = 0
    endif
endfun

"""""""""""""""""""""""""""""""""""""""""""" 按下 F5 自动编译执行
nnoremap <F5> :call Compile()<cr>
func! Compile()
    exec "w"
    if &filetype == 'c'
      exec "!gcc -g % -o %<"
      exec "!./%<"
    elseif &filetype == 'cpp'
      exec "!gcc -g % -o %<"
      exec "!./%<"
    elseif &filetype == 'python'
      exec "!python %"
    endif
endfunc

""""""""""""""""""""""""""""""""""""""""""""" a.vim
" 快速切换 .h 和 .c
nnoremap <space>t :AT<cr>

""""""""""""""""""""""""""""""""""""""""""""" syntastic(可以被ale或者YCM替换)
" 语法检查
" 设置error和warning的标志
let g:syntastic_enable_signs = 1
let g:syntastic_error_symbol='E>'
let g:syntastic_warning_symbol='W>'
"总是打开Location List（相当于QuickFix）窗口，如果你发现syntastic因为与其他插件冲突而经常崩溃，将下面选项置0
let g:syntastic_always_populate_loc_list = 1
"自动打开Locaton List，默认值为2，表示发现错误时不自动打开，当修正以后没有再发现错误时自动关闭，置1表示自动打开自动关闭，0表示关闭自动打开和自动关闭，3表示自动打开，但不自动关闭
let g:syntastic_auto_loc_list = 1
"修改Locaton List窗口高度
let g:syntastic_loc_list_height = 5
"打开文件时自动进行检查
let g:syntastic_check_on_open = 1
"自动跳转到发现的第一个错误或警告处
let g:syntastic_auto_jump = 1
"进行实时检查，如果觉得卡顿，将下面的选项置为1
let g:syntastic_check_on_wq = 1
"高亮错误
let g:syntastic_enable_highlighting=1
"让syntastic支持C++11
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'
"设置pyflakes为默认的python语法检查工具
let g:syntastic_python_checkers = ['pyflakes']
"修复syntastic使用:lnext和:lprev出现的跳转问题，同时修改键盘映射使用sn和sp进行跳转
function! <SID>LocationPrevious()                       
  try                                                   
    lprev                                               
  catch /^Vim\%((\a\+)\)\=:E553/                        
    llast                                               
  endtry                                                
endfunction                                             
function! <SID>LocationNext()                           
  try                                                   
    lnext                                               
  catch /^Vim\%((\a\+)\)\=:E553/                        
    lfirst                                              
  endtry                                                
endfunction                                             
nnoremap <silent> <Plug>LocationPrevious    :<C-u>exe 'call <SID>LocationPrevious()'<CR>                                        
nnoremap <silent> <Plug>LocationNext        :<C-u>exe 'call <SID>LocationNext()'<CR>
nmap <silent> sp    <Plug>LocationPrevious              
nmap <silent> sn    <Plug>LocationNext
"关闭syntastic语法检查, 鼠标复制代码时用到, 防止把错误标志给复制了
nnoremap <silent> <Leader>ec :SyntasticToggleMode<CR>
function! ToggleErrors()
    let old_last_winnr = winnr('$')
    lclose
    if old_last_winnr == winnr('$')
        " Nothing was closed, open syntastic error location panel
        Errors
    endif
endfunction

""""""""""""""""""""""""""""""""""""""""""""""" OmniCpp 代码补全(可以被YCM替代)
let OmniCpp_SelectFirstItem = 2
set completeopt=menu,menuone  
let OmniCpp_MayCompleteDot=1    " 打开  . 操作符
let OmniCpp_MayCompleteArrow=1  " 打开 -> 操作符
let OmniCpp_MayCompleteScope=1  " 打开 :: 操作符
let OmniCpp_NamespaceSearch=1   " 打开命名空间
" set completeopt=longest,menu
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"
let g:SuperTabDefaultCompletionType="context" 
let g:SuperTabDefaultCompletionType="<c-x><c-o>"
let g:SuperTabRetainCompletionType=2

""""""""""""""""""""""""""""""""""""""""""""""" SnipMate(可以被UltiSnips替代)
" 替换触发代码模板的快捷键. 但是替换后会出现不能正确选中变量的 bug. 暂时不开启
" imap <C-J> <Plug>snipMateNextOrTrigger 
" smap <C-J> <Plug>snipMateNextOrTrigger
