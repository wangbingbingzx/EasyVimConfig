# vim配置全攻略

### 写在前面
折腾有风险, 作死请谨慎.

善用虚拟机的快照功能, 能够节省大把的时间.

# EasyVimConfig(vim低配版)      [centos6 i686 推荐]
## 简介

一键式安装, 开箱即用. 兼容性较好. 部分功能较弱.

## 折腾程度
0星

## 安装方法

    curl -sLf https://gitee.com/HGtz2222/EasyVimConfig/raw/master/install.sh -o /tmp/install.sh && bash /tmp/install.sh

为了自动安装一些依赖的工具(unzip, curl等), 安装过程需要输入 root 密码. 您的 root 密码不会被上传. 请放心输入.

## 支持的系统
系统版本 centos6, centos7 均可. 支持 i686 和 x86_64, . vim版本 >= 7.0 即可.

## vim配置
由于 centos6.5 i686 对应的vim和gcc环境都比较老, 因此很多新的插件并不支持. 尤其是代表性的 YouCompleteMe 插件. 因此我们为了简单不折腾, 推荐了以下插件集合,
能够比较好的兼容旧版本的vim

### 插件集合

>* auto-pairs.vim: 括号自动匹配
>* a.vim: 切换.h/.c文件
>* DoxygenToolkit.vim: 生成文档风格的注释
>* mark.vim: 高亮关键字
>* NERD_tree.vim: 文件列表
>* tagbar.vim: 函数列表
>* ctrlp.vim: 快速查找文件
>* gruvbox: 一个漂亮的主题
>* python-mode: 一组Python开发的工具集合
>* vim-airline: 漂亮的标签页和状态栏
>* vim-colorschemes: 一个主题集合包
>* vim-cpp-enhanced-highlight: 更精细的c/c++语法高亮
>* vim-surround: 快速给一段代码外面包裹括号/引号
>* Vundle.vim: vim的一个插件管理器
>* AutoComlPop + OmniCppComplete + SuperTab + ctags: 代码补全
>* ctags: 代码跳转(这货不是vim插件, 是一个linux工具)
>* syntastic: 语法检查
>* snipMate + vim-snippets: 代码片段

### 使用方法

详细的使用方法, 可以参考 ~/.vimrc 中的注释内容

# vim中配版(1) -- VimForCpp     [centos7 推荐]
## 简介
VimForCpp 是我目前在使用的配置.

这份配置目前解决了三个痛点问题:
>* 安装简单, 一键式安装, 拒绝折腾
>* 安装速度快, 网络畅通的情况下2分钟内完成安装.
>* 不必手动编译 YouCompleteMe 就能使用其强大功能.

目前只支持 centos7 x86_64

官网参见码云

    https://gitee.com/HGtz2222/VimForCpp

## 折腾程度
0星

## 快速安装

    curl -sLf https://gitee.com/HGtz2222/VimForCpp/raw/master/install.sh -o /tmp/install.sh && bash /tmp/install.sh

需要按照提示输入 root 密码. 您的 root 密码不会被上传, 请放心输入.

## 使用方法
参考码云上的文档

# vim中配版(2) -- vimplus
## 简介
vimplus是国内的一个大佬打造的一个vim配置

官网参见github

    https://github.com/chxuan/vimplus

## 折腾程度
安装过程: 4星

## 安装过程
可以参考github上的文档

	git clone https://github.com/chxuan/vimplus.git ~/.vimplus
	cd ~/.vimplus
	./install.sh

由于 vimplus 内置了 YouCompleteMe, 并且会自动进行编译, 省了不少心. 安装过程比较久, 请耐心等待.
但是仍然不能保证100%安装成功. 如果安装失败, 请自行百度.

![](https://i.imgur.com/E5bgZhn.jpg)

## 使用方法
官方github上有详细说明. 比spacevim简洁了不少.

# vim高配版(1) -- SpaceVim
## 简介
SpaceVim 是国内的一个大佬将一些NB的插件整合到一起的一个插件包. 一键式安装, 功能强大.

官网参见 https://spacevim.org/

## 折腾程度
安装过程: 5星

需要手动安装一些依赖的软件. 有可能需要通过源码编译安装.

## 软件依赖

vim7.4+ 或 neovim 0.2.0 以上.

centos7 默认 vim 版本为 7.4, 默认 gcc 版本为4.8, 比centos6.5好了不少. 但还是不够. 要想装一些新版的插件, 仍然需要升级 vim(甚至gcc)

为了避免编译, 可以通过 yum 安装 neovim 以及 neovim 的 python 扩展来解决这个问题

下面介绍 centos7 x86_64 上安装 SpaceVim 的过程.

## 安装过程
### 安装 git
参考附录一

### 安装 neovim
参考附录二

### 安装 SpaceVim
下载安装基本配置

	curl -sLf https://spacevim.org/install.sh | bash

效果如图

![](https://i.imgur.com/Iv3VPcj.jpg)

下载安装插件.

启动 vim, 等待插件下载完成. 由于插件数目较多, 因此下载时间也会很长. 万一下载中途出现界面卡死进度条下载缓慢的情况, 可以重启vim再次尝试下载.

![](https://i.imgur.com/aXfCGO0.jpg)

安装成功后, 打开会看到欢迎界面

但是这时候如果是使用 xshell 连接的话, 可能会出现颜色不正确的情况. 可以参考下面的解决方法.

## 使用方法
SpaceVim包含的插件体系非常丰富, 种类繁多. 因此在使用方面上作者有自己的一套设计体系. 
具体可以参考官网文档.

## 一些问题

### XShell 下颜色显示混乱问题

在 .SpaceVim.d/init.vim 中添加以下两句配置

	let g:spacevim_enable_guicolors = 0
	set t_Co=256

### 相对行号问题
如果有些老铁不习惯相对行号, 可以将相对行号关闭.

![](https://i.imgur.com/NX72Vp7.png)

	set norelativenumber

# vim高配版(2) -- spf13
## 简介
国外一个大佬的一份 vim 配置, 能够比较方便的安装.

参见官网
    
    http://vim.spf13.com/

## 安装方法

    curl http://j.mp/spf13-vim3 -L -o - | sh

# vim作死版 -- 尝试手动安装 YouCompleteMe

## 简介
YCM是vim史上最NB插件, 没有之一. 同时也是史上最难安装的插件, 也没有之一.

具备以下非常屌炸天的功能
>* 精准的语义补全
>* 客户端服务器异步IO架构, 非常高的性能以及非常流畅的用户体验.
>* 语法检查
>* 模糊匹配
>* 跳转到声明/定义
>* 进入 include 的头文件
>* 支持很多其他的主流编程语言(不过C++支持的最好) 

## 软件依赖
vim8.0或者neovim 0.2.0

**只支持x86_64系统**.

## 折腾程度
安装过程: 9星

当前网络上的大部分 YCM 安装的博客, 按照上面的步骤操作 "十有八九" 都**不会成功**.

要想手动安装成功, 必须有很大的耐心, 最准确最权威的资料就是读 github 上的官方文档, 以及遇到问题的时候使用 google(不要用baidu).

安装难点主要在这几个方面:
>* vim版本限制. 需要升级vim. 最好是vim8.0或者neovim
>* gcc版本限制. 需要升级gcc.
>* 编译ycm_core. 这个是补全cpp代码的核心模块. 编译过程中可能会出现很多问题. 尤其是依赖的编译工具缺失. 如果遇到问题, 仔细阅读错误日志, 并且google(不要用baidu)
>* .ycm_extra_conf.py 的配置. 需要对其中的内容进行一定的修改和调整. 需要理解c++编译器的一些基本知识.
>* 安装完毕后, 不能补全代码: 需要使用 :YcmDebugInfo 指令, 找到对应的错误日志文件. 然后根据错误日志中的信息, 结合google和YCM源码分析解决. 这个只能见招拆招. 

所以建议还是使用 vimplus 能够帮助我们解决一部分问题.

# 附录一: 手动编译安装 git

如果是 centos7 可以直接 yum install git 即可.

但是如果是 centos6, 默认的 git 版本太低(1.7.1), 需要手动编译

## 源码安装 git

先安装编译时依赖的包(以下操作需要 root 用户)
	
	yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel
	yum install  gcc perl-ExtUtils-MakeMaker
	yum install  perl-ExtUtils-MakeMaker package

为了预防以下问题

	fatal: unable to access 'https://github.com/VundleVim/Vundle.vim.git/': SSL connect error

执行

	yum update openssh nss curl libcurl

从该链接上下载源码, 并解压缩
	
	cd ~
	wget https://www.kernel.org/pub/software/scm/git/git-2.0.5.tar.gz
	tar xzf git-2.0.5.tar.gz

进入源码, 开始编译.
	
	cd git-2.0.5
	make prefix=/usr/local/git all
	make prefix=/usr/local/git install

运行以下指令将新编译好的 git 放到 PATH 环境变量中

	echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
	source /etc/bashrc

检查安装结果:

	git --version

观察版本号是否符合预期(2.0.5). 
	
安装过程参考 https://www.cnblogs.com/fuyuanming/p/5804695.html 

# 附录二: 安装 neovim
**注意, 仅限于 centos7 可以通过 yum 源安装. 通过源码安装 neovim 此处不介绍. 参考官网文档**

先安装yum源的扩展

	yum -y install epel-release

然后安装 neovim

	yum install -y neovim.x86_64 python2-neovim.noarch

最后定义别名. 输入 vim 启动 neovim

	# 修改 /etc/bashrc
	alias vim="nvim"

然后重启终端后生效. 此时敲下 vim, 启动的实际是 nvim. 
