install_user_home=$1
function InstallEnv() {
    # 检查操作系统版本是否 ok
    version_ok=`uname -a | awk '{ if (index($0, "el7") > 0 || index($0, "el6")) print 1; else print 0; }'`
    if [ $version_ok -eq 0  ]; then
        echo "操作系统版本不支持! 目前只支持 centos6 及以上版本!"
        exit 1
    fi
    # 检查 zip 工具是否安装
    unzip --help > /dev/null
    if [ $? -ne 0 ]; then
        yum -y install zip
    fi
    unzip --help > /dev/null
    if [ $? -ne 0 ]; then
        echo "unzip 安装失败!"
        exit 1
    fi
    curl --version > /dev/null
    if [ $? -ne 0 ]; then
        yum -y install curl
    fi
    curl --version > /dev/null
    if [ $? -ne 0 ]; then
        echo "curl 安装失败!"
        exit 1
    fi
    ctags --version > /dev/null
    if [ $? -ne 0 ]; then
        yum -y install ctags
    fi
}

function DownloadVimConfig() {
    if [ -d ./EasyVimConfig ]; then
        rm -rf ./EasyVimConfig
    fi
    rm -f /tmp/EasyVimConfig.zip
    curl "https://gitee.com/HGtz2222/EasyVimConfig/repository/archive/master.zip" -o /tmp/EasyVimConfig.zip
    unzip /tmp/EasyVimConfig.zip
    if [ $? -ne 0 ]; then
        echo "Vim 配置解压缩失败! 检查网络是否畅通, 并尝试重新安装!"
        exit 1
    fi
    rm -f /tmp/EasyVimConfig.zip
    echo "Vim 配置下载完毕!"
}

function DownloadPlugin() {
    curl "https://gitee.com/HGtz2222/vim-plugin-fork/repository/archive/master.zip" -o /tmp/vim-plugin-fork.zip
    unzip /tmp/vim-plugin-fork.zip
    if [ $? -ne 0 ]; then
        echo "依赖插件解压缩失败! 检查网络是否畅通, 并尝试重新安装!"
        exit 1
    fi
    mv ./vim-plugin-fork/* ./EasyVimConfig/vim/bundle/
    if [ $? -ne 0 ]; then
        echo "依赖插件拷贝失败! 请尝试重新安装!"
        exit 1
    fi
    rm -rf ./vim-plugin-fork
    rm -rf /tmp/vim-plugin-fork.zip
    echo "Vim 依赖插件安装完毕!"
}

function LinkDir() {
    # 先备份原有的 vim 配置文件
    today=`date +%m%d`
    mv $install_user_home/.vim $install_user_home/.vim.bak_${today}
    mv $install_user_home/.vimrc $install_user_home/.vimrc.bak_${today}
    # 建立链接
    target_dir=`pwd`"/EasyVimConfig"
    ln -s $target_dir/vim $install_user_home/.vim
    ln -s $target_dir/vimrc $install_user_home/.vimrc
    # 修改权限
    install_user=`echo $install_user_home | awk -F '/' '{print $3}'`
    chown -R $install_user:$install_user $target_dir
    chown -R $install_user:$install_user $install_user_home/.vim
    chown -R $install_user:$install_user $install_user_home/.vimrc
}

# 1. 检查并安装依赖的软件
InstallEnv
# 2. 从码云上下载 vim 配置
DownloadVimConfig
# 3. 从码云上下载依赖的插件
DownloadPlugin
# 4. 备份对应用户的 .vim 目录, 并建立好连接以及修改好权限
LinkDir
echo "安装完毕!"
