#!/usr/bin/sh
echo "
+------------------------------------------------------+
|                                                      |
|                   EasyVimConfig                      |
|                                                      |
+------------------------------------------------------+
安装开始!
整个过程大概需要下载大约 50MB 数据.
"
# 切换到 root 用户, 并且触发安装脚本
function SwitchRoot() {
  echo "为了安装依赖程序, 需要使用 root 账户. 您的密码不会被上传."
  echo "请输入 root 密码:"
  su -c "curl -sLf https://gitee.com/HGtz2222/EasyVimConfig/raw/master/start.sh -o /tmp/start.sh && bash /tmp/start.sh $HOME"
}

# 切换到 root 用户
SwitchRoot

